# README
## CODEOWNERS
If you add someone to `CODEOWNERS` and they are not a member of the repository, they will not show up in the list of 'Code Owners'. Once you add them to the repository as a member, they will appear in the list of 'Code Owners' in the Web interface.

## Issues
### Importing Issues
Where can one find sample data?
See [https://github.com/karolzak/support-tickets-classification#22-dataset](https://github.com/karolzak/support-tickets-classification#22-dataset). 

There is a link to a [CSV](https://privdatastorage.blob.core.windows.net/github/support-tickets-classification/datasets/all_tickets.csv). 

Also see [misc/issues-to-import.csv](misc/issues-to-import.csv) in this very repository.

A note on importing issues via CSV. A header row must exist. The title of the first column in the header row must be "title"; note that this string is case-insensitive. The title of the second column in the header row must be "description"; note that this string is also case-insensitive.  

Imagine a CSV like this:

```
title,message
"server down","Please help!"
"can't send email","I get a 'relay access denied' error."
```

That would create two issues with titles but no body. If there is Markdown in the description field, it will be rendered. :unicorn:

## GitLab classification
```
rules	List of conditions to evaluate and determine selected attributes of a job, 
and whether or not it’s created. May not be used alongside only/except.
```
Thu Jul 16 18:00:35 EDT 2020
Thu Jul 16 18:02:14 EDT 2020
Thu Jul 16 18:03:01 EDT 2020
Thu Jul 16 19:05:41 EDT 2020
Thu Jul 16 19:07:28 EDT 2020
Thu Jul 16 19:09:56 EDT 2020
Thu Jul 16 19:16:48 EDT 2020
Thu Jul 16 19:25:04 EDT 2020
Thu Jul 16 19:31:18 EDT 2020
Thu Jul 16 19:37:51 EDT 2020
Thu Jul 16 19:38:03 EDT 2020
Thu Jul 16 19:38:30 EDT 2020
Thu Jul 16 19:38:32 EDT 2020
Thu Jul 16 19:38:32 EDT 2020
Thu Jul 16 19:38:38 EDT 2020
Thu Jul 16 19:54:28 EDT 2020
Thu Jul 16 19:54:42 EDT 2020
Thu Jul 16 20:12:36 EDT 2020
Thu Jul 16 20:12:53 EDT 2020
Thu 16 Jul 2020 09:03:37 PM EDT
Fri Jul 17 14:53:29 EDT 2020
Fri Jul 17 14:59:30 EDT 2020
